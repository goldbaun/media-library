﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrary
{
    // Przechowuje podstawowe informacje o utworze
    public class Song
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Album { get; set; }

        public int Playtime { get; set; }

        [ForeignKey("Artist")]
        public int ArtistID { get; set; }

        public virtual Artist Artist { get; set; }
    }
}
