﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MusicLibrary
{
    /// Logika interakcji dla klasy MainWindow.xaml
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            IList<Playlist> pList;
            using (var context = new MLibraryContext())
            {
                pList = context.Playlists.ToList();
            }

            playList.ItemsSource = pList;
            Console.WriteLine(playList);
        }

        private void btnShowSelectedItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show((playList.SelectedItem as Playlist).Title);
        }
    }
}
