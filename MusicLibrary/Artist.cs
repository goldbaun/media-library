﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrary
{
    // Przechowuje podstawowe informacje o artyście, możemy dodawać kolejne utwory do listy 
    public class Artist
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        [ForeignKey("Playlist")]
        public int PlaylistID { get; set; }

        public virtual Playlist Playlist { get; set; }

        public virtual ICollection<Song> Songs { get; set; }
    }
}
