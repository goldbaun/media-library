using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Collections.ObjectModel;

namespace MusicLibrary
{
    public class MLibraryContext : DbContext
    {

        public MLibraryContext()
            : base("name=MLibraryContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Song> Songs { get; set; }

        public void AddNewPlaylist(Playlist playlist)
        {
            using (var context = new MLibraryContext())
            {
                context.Playlists.Add(playlist);
                context.SaveChanges();
            }
        }

        public void AddNewArtist(Artist artist)
        {
            using (var context = new MLibraryContext())
            {
                context.Artists.Add(artist);
                context.SaveChanges();
            }
        }

        public void AddNewSong(Song song)
        {
            using (var context = new MLibraryContext())
            {
                context.Songs.Add(song);
                context.SaveChanges();
            }
        }

        public void DeletePlaylist(Playlist playlist)
        {
            using (var context = new MLibraryContext())
            {
                context.Playlists.Remove(playlist);
                context.SaveChanges();
            }
        }

        public void RemoveArtist(Artist artist)
        {
            using (var context = new MLibraryContext())
            {
                context.Artists.Remove(artist);
                context.SaveChanges();
            }
        }

        public void RemoveSong(Song song)
        {
            using (var context = new MLibraryContext())
            {
                context.Songs.Remove(song);
                context.SaveChanges();
            }
        }
    }

    public class MLibraryDbInitializer : DropCreateDatabaseAlways<MLibraryContext>
    {

        protected override void Seed(MLibraryContext context)
        {
            var playlists = new List<Playlist>
                {
                    new Playlist()
                    { 
                        ID=1,
                        Title="Rock",
                        Artists = new Collection<Artist>
                        {
                            new Artist()
                            {
                                ID=1001,
                                Name="Linkin Park",
                                PlaylistID = 1,
                                Image = "",
                                Songs = new Collection<Song>
                                {
                                    new Song()
                                    {
                                        ID = 2001,
                                        Title = "Numb",
                                        Album = "Meteora",
                                        Playtime = 120,
                                        ArtistID = 1001
                                    },
                                    new Song()
                                    {
                                        ID = 2002,
                                        Title = "Papercut",
                                        Album = "Hybrid Theory",
                                        Playtime = 120,
                                        ArtistID = 1001
                                    }
                                }
                            }
                        }
                    },

                    new Playlist() 
                    { 
                        ID=2, 
                        Title="Rap",
                        Artists = new Collection<Artist>
                        {
                            new Artist()
                            {
                                ID=1002,
                                Name="Kizo",
                                Image = "",
                                PlaylistID = 2,
                                Songs = new Collection<Song>
                                {
                                    new Song()
                                    {
                                        ID = 2003,
                                        Title = "MTS",
                                        Album = "Jeszcze 5 minut",
                                        Playtime = 120,
                                        ArtistID = 1002
                                    },
                                    new Song()
                                    {
                                        ID = 2004,
                                        Title = "Tsunami",
                                        Album = "Posejdon",
                                        Playtime = 120,
                                        ArtistID = 1002
                                    }
                                }
                            }
                        }
                    },

                    new Playlist() 
                    { 
                        ID=3, 
                        Title="Pop",
                        Artists = new Collection<Artist>
                        {
                            new Artist()
                            {
                                ID=1003,
                                Name="Ed Sheeran",
                                Image = "",
                                Songs = new Collection<Song>
                                {
                                    new Song()
                                    {
                                        ID = 2005,
                                        Title = "One",
                                        Album = "X",
                                        Playtime = 120,
                                        ArtistID = 1003
                                    },
                                    new Song()
                                    {
                                        ID = 2006,
                                        Title = "Shape of You",
                                        Album = "Divide",
                                        Playtime = 120,
                                        ArtistID = 1003
                                    }
                                }
                            }
                        }
                    }
            };

            playlists.ForEach(p => context.Playlists.Add(p));
            context.SaveChanges();
        }
    }

}