namespace MusicLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Artists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Image = c.String(),
                        PlaylistID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Playlists", t => t.PlaylistID, cascadeDelete: true)
                .Index(t => t.PlaylistID);
            
            CreateTable(
                "dbo.Playlists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Songs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Album = c.String(),
                        Playtime = c.Int(nullable: false),
                        ArtistID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Artists", t => t.ArtistID, cascadeDelete: true)
                .Index(t => t.ArtistID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Songs", "ArtistID", "dbo.Artists");
            DropForeignKey("dbo.Artists", "PlaylistID", "dbo.Playlists");
            DropIndex("dbo.Songs", new[] { "ArtistID" });
            DropIndex("dbo.Artists", new[] { "PlaylistID" });
            DropTable("dbo.Songs");
            DropTable("dbo.Playlists");
            DropTable("dbo.Artists");
        }
    }
}
