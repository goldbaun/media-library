﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicLibrary
{
    // Przechowuje podstawowe informacje o liście artystów
    public class Playlist
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public virtual ICollection<Artist> Artists { get; set; }
    }
}
